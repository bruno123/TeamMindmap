/**
 * Created by spatra on 14-12-2.
 */


define(['commonJS/module'], function(commonModule){

  commonModule.directive('mainTopNav',['$rootScope', '$state', 'commonModuleBaseUrl', 'LoginStatusService',
    function($rootScope, $state, commonModuleBaseUrl, LoginStatusService){

      return {
        restrict: 'E',
        templateUrl: commonModuleBaseUrl + 'tpls/mainTopNav.html',
        scope: {
          functionNavItems: '=functionNavItems'
        },
        link: function(scope, element, attr){
          /*
          对传递进来的栏目进行预处理
           */
          var functionNavItemsSet = {}, lastItem = {};
          for(var i = 0 ; i < scope.functionNavItems.length; ++i ){
            functionNavItemsSet[ scope.functionNavItems[i]['name'] ] = scope.functionNavItems[i];
          }

          scope.personalInfo = LoginStatusService.get('personalInfo');  //获取用户数据
          scope.logoutUri = LoginStatusService.get('uri.logout'); //获取退出的路径
          scope.unread = LoginStatusService.get('unread');  //未读的私信或通知的统计信息

          /*
          高亮当前栏目
           */
          function highLightItem(state){
            lastItem['active'] = false;

            var currItem = state.name.split('.')[0];

            if( currItem ){
              functionNavItemsSet[ currItem ].active = true;
              lastItem = functionNavItemsSet[ currItem ];
            }
          }

          /*
          监听路由事件，自动高亮当前栏目
           */
          $rootScope.$on('$stateChangeSuccess', function(evt, toState, toParams){
            highLightItem(toState);
          });
        }
      };
  }]);

  commonModule.directive('readStatusSwitch',function(){
    return {
      restrict: 'EA',
      replace: true,
      template: '<div)">' +
      '<span class="unread" ng-class="{\'active\': ! readCondition}" ng-click="toggle()">{{offLabel}}</span>' +
      '<span class="read" ng-class="{\'active\': readCondition}" ng-click="toggle()">{{onLabel}}</span>' +
      '</div>',
      scope: {
        onLabel: '@',
        offLabel: '@',
        readCondition: '='
      },
      link: function(scope){
        scope.toggle = function () {
          scope.readCondition = ! scope.readCondition;
        };


      }
    };
  });//End of --> readStatusSwitch

  /**
   * 此指令用于按条件过滤
   */
  commonModule.directive('filterOnCond', ['commonModuleBaseUrl', 'ClassHelperService',
    function(baseUrl, ClassHelperService){

      return {
        restrict: 'EA',
        replace: true,
        templateUrl: baseUrl + 'tpls/filter-on-cond.html',
        scope:{
          conditionObj: '=',
          conditions: '='
        },
        link: function(scope){
          //为避免选择标记影响原有对象，故此处使用深度赋值后的对象
          scope.conditions = ClassHelperService.clone(scope.conditions);

          var lastItems = {};
          for( var item in scope.conditions ){
            lastItems[item] = scope.conditions[item][0];
            lastItems[item].selected = true;
          }

          scope.setSelected = function(currentCond, condName){
            lastItems[condName].selected = false;
            currentCond.selected = true;
            lastItems[condName] = currentCond;

            scope.conditionObj[condName] = currentCond.cond;
          };
        }
      };
    }]);//End of --> filterOnCond

  /**
   *  指令，模块使用的三级导航栏
   */
  commonModule.directive('moduleThirdNav', ['$rootScope', '$state', 'commonModuleBaseUrl',
    function($rootScope, $state, commonModuleBaseUrl){

      return {
        restrict: 'EA',
        templateUrl: commonModuleBaseUrl + 'tpls/module-third-nav.html',
        scope: {
          navItems: '=',
          parentStateName: '@'
        },
        link: function(scope){
          scope.navItemSet = {};
          //讲导航项目列表转化成k-v对的集合
          scope.navItems.forEach(function(currNavItem){
            scope.navItemSet[ currNavItem['uiSref'] ] = currNavItem;
          });

          var lastItem = {};
          function highLight(stateName){
            lastItem['active'] = false;

            if( scope.navItemSet[ stateName ] ){
              lastItem = scope.navItemSet[ stateName ];
              lastItem['active'] = true;
            }
          }

          highLight($state.current.name);

          $rootScope.$on('$stateChangeSuccess', function(event, toState){
            if( toState.name.indexOf(scope.parentStateName) !== -1 ){
              highLight(toState.name);
            }
          });
        }
      };
    }]);//End of --> moduleThirdNav
});