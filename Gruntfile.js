module.exports = function(grunt) {
  //配置项
  grunt.initConfig({
    bower: {
    install: {
      options: {
        targetDir: './public/js/lib',
        layout: 'byType',
        install: true,
        verbose: false,
        cleanTargetDir: false,
        cleanBowerDir: false,
        bowerOptions: {}
      }
    }
  }
  });


grunt.loadNpmTasks('grunt-bower-task');

grunt.registerTask('default', ['bower']);

};