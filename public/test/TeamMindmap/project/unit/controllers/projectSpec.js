/**
 * Created by spatra on 14-12-4.
 */


define(['angular', 'projectJS/controllers/project', 'angularMocks'], function(angular){

  describe('Unit: 项目模块controllers测试', function() {

    /*
      初始化 Project Module，以及对一些用到的内置服务进行初始化
     */
    beforeEach(module('TeamMindmap.project'));

    var $controller, $rootScope, $httpBackend, $stateParams;
    beforeEach(inject(function(_$controller_, _$rootScope_, _$stateParams_, _$httpBackend_){
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      $stateParams = _$stateParams_;
    }));

    // End of --> `初始化 Project Module，以及对一些用到的内置服务进行初始化`

    describe('ProjectDesktopController', function(){
      var ProjectDesktopController, scope;

      //创建一个模拟的modal对象
      var fakeModal = {
        result: {
          then: function(confirmCallback,cancelCallback) {
            this.confirmCallback = confirmCallback;
            this.cancelCallback = cancelCallback;
          }
        },

        close: function(item){
          this.result.confirmCallback(item);
        },
        dismiss: function(type){
          this.result.cancelCallback(type);
        }
      };

      beforeEach(inject(function($modal){
        //监听$modal的open方法
        spyOn($modal,'open').andReturn(fakeModal);
      }));



      beforeEach(inject(function(_$modal_){
        //为了给TaskService设置projectId,通过$stateParams服务设置路由的projectId
        $stateParams.projectId = 12;
        scope = $rootScope.$new();
        scope.handleNode = {
          addNode: function(name, id){}
        };

        ProjectDesktopController = $controller('ProjectDesktopController',{
          $scope: scope,
          $modal: _$modal_,
          taskSet: {},
          currentProjectInfo: {}
        });

      }));

      it('调用createTask方法后,当触发modalInstance的close后,应该请求新增一个任务',function(){
        scope.createTask();

        $httpBackend.expectPOST('api/project/12/task').respond(200,{
          id: 2
        });
        scope.createTaskModalInstance.close({
          name: 'mockTaskName',
        });
        $httpBackend.flush();


      });


    });//End of --> ProjectDesktopController

    describe('ProjectSettingController',function(){
      var ProjectSettingController, scope;
      beforeEach(function(){
        $stateParams.projectId = 12;
        scope = $rootScope.$new();
        ProjectSettingController = $controller('ProjectSettingController',{
          $scope: scope,
          projectInfo: {}
        })
      });
      it('test',function(){
        console.log(scope.projectData);
      });

      it('测试方法: submitProject',function(){
        var postData = {
          name: 'aProject',
          introduction: 'hello',
          cover: 'fa-gear'

        };
        scope.projectData = postData;
        $httpBackend.expectPUT('api/project/12',postData).respond(200);
        scope.submitProject();

        $httpBackend.flush();
      });

      //it('测试方法: deleteProject',function(){
      //
      //  $httpBackend.expectDELETE('api/project/12').respond(200);
      //  scope.deleteProject();
      //  $httpBackend.flush();
      //
      //});

    });//End of --> ProjectSettingController

    describe('ProjectCreatingController',function(){

    });//End of --> ProjectCreatingController




  });
});