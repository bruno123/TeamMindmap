/**
 * Created by spatra on 14-12-2.
 */

define(['angular', 'commonJS/ng-require', 'personalNotificationJS/ng-require', 'personalMessageJS/ng-require', 'angularUIRouter', 'localResize'],
  function(angular, commonModule, personalNotificationModule, personalMessageModule){


    var personalModule = angular.module('TeamMindmap.personal', [commonModule, personalNotificationModule, personalMessageModule, 'ui.router','localResizeIMG']);

    personalModule.constant('personalModuleBaseUrl', 'ngApp/personal/');

    return personalModule;
});