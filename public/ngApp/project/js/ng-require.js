/**
 * Created by spatra on 14-12-2.
 */


define(['projectJS/module',
    'projectJS/controllers/project', 'projectJS/controllers/ngUI', 'projectJS/controllers/task', 'projectJS/controllers/member', 'projectJS/controllers/discussion', 'projectJS/controllers/sharing',
    'projectJS/services', 'projectJS/directives'],
  function(projectModule){
    return projectModule.name;
});
