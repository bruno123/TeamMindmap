/**
 * Created by Dick on 14/12/12.
 */
define(['angular', 'commonJS/ng-require', 'projectJS/ng-require', 'markdownEditorJS/ng-require', 'angularBootstrap', 'angularBootstrapTemplate'],
  function(angular, commonModule, projectModule, mdEditorModule){

  var messageModule = angular.module('TeamMindmap.personal.message',[commonModule, projectModule, mdEditorModule,'ui.bootstrap', 'ui.bootstrap.tpls']);

  return messageModule;
});