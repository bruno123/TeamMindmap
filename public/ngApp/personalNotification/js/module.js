/**
 * Created by rockyren on 14/12/11.
 */

/**
 *
 */
define(['angular', 'commonJS/ng-require'],function(angular, commonModule){
  var notificationModule = angular.module('TeamMindmap.personal.notification',[commonModule]);

  return notificationModule;
});