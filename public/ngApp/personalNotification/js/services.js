/**
 * Created by spatra on 14-12-12.
 */

define(['personalNotificationJS/module'], function(notificationModule){

  /**
   * 用于`通知`相关的服务
   */
  notificationModule.factory('NotificationService', ['ResourceService',
    function(ResourceService){

      return {
        accessor: ResourceService.getResourceAccessor({
          resourceName: 'notify'
        }),
        setRead: function(notificationId, readStatus){
          if( readStatus === undefined ){
            readStatus = true;
          }

          var self = this;

          return self.accessor.update(notificationId, {
            "read": readStatus
          });
        },
        getStateObj: function(notification, typeLabelSet){
          var projectTypeId = 2;

          var stateObj = {
            stateName: '',
            stateParams: {}
          };

          if(notification.type_id == projectTypeId){
            stateObj.stateName = 'project.show.desktop';
            stateObj.stateParams['projectId'] = [notification.project_id];

          }else{
          //由type_id得到stateName
            var curTypeLabel = typeLabelSet[notification.type_id];
            stateObj.stateName = 'project.show.' + curTypeLabel + '.info';
            stateObj.stateParams['projectId'] = notification.project_id;
            var sourceIdName = curTypeLabel + 'Id';
            stateObj.stateParams[sourceIdName] = notification.source_id;

          }

          //由notification的projectId,sourceId得到stateParams;

          return stateObj;
        }
        /**
         * 按是否已读的过滤通知数组
         *
         * @param source  源数据
         * @param readStatus 阅读状态
         * @returns {Array}
         */
          /*
        filterOnReadStatus: function(source, readStatus){
          var result = [];

          source.forEach(function(item){
            if( item.read == readStatus ){
              result.push(item);
            }
          });

          return result;
        },
        //根据指定的条件( projectId， read），得到指定通知
        getNotificationsOnCond: function(conditionObj, scope, scopeNotificationsProperty){
          scopeNotificationsProperty = scopeNotificationsProperty || 'notificationList';


          var self = this;
          var currentProjectId = conditionObj['projectId'], currentReadStatus = conditionObj['read'];

          if( currentProjectId == self.lastProjectId && self.lastNotificationsSource){
            // 如果项目id并没有改变，则直接通过原有数据进行过滤
            scope[ scopeNotificationsProperty ] = self.filterOnReadStatus(self.lastNotificationsSource, currentReadStatus);
          }
          else{
            // 如果项目id改变了，又或者本身没有记录原数据，则进行后台获取
            self.getNotificationOnProject(currentProjectId)
              .then(function(resp){
                self.lastNotificationsSource = resp.data;
                scope[ scopeNotificationsProperty ] = self.filterOnReadStatus(resp.data, currentReadStatus);
              },function(resp){
                console.error(resp.data);
                scope.$emit('message:error', {
                  title: '通知加载失败',
                  msg: resp.data.error
                })
              });
          }

          self.lastProjectId = currentProjectId;
        },
        //根据指定的项目获取id
        getNotificationOnProject: function(projectId){
          var self = this;

          return self.accessor.get({
            params: {project_id: projectId}
          });
        }
        */
      };
  }]);// End of --> NotificationService

  notificationModule.factory('NotificationTypeService', ['ResourceService', function(ResourceService){
    return {
      accessor: ResourceService.getResourceAccessor({
        'resourceName': 'notify-type'
      }),
      getTypeLabelSet: function(){
        return this.accessor.get()
          .then(function(resp){
            var typeList = resp.data;

            var typeLabelSet = {};

            for(var i=0; i<typeList.length; i++){
              var type = typeList[i];

              var splitName = type.name.split('_');
              var typeName = splitName[splitName.length-1];


              typeLabelSet[type.id] = typeName;
            }

            return typeLabelSet;

          },function(resp) { console.log(resp); })
      }

    }
  }]);


});