<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14-12-25
 * Time: 下午1:47
 */
namespace Libraries\EventHandler;

use Auth;
use Notification;
use Project;
use Project_Member;
use ProjectDiscussionFollower;

class ProjectDiscussionEventHandler extends AbstractEventHandler
{

    /**
     * 新建讨论触发通知创建、通知发送
     * @param $event
     */
    public function discussionCreated($event)
    {
        $title = $event['title'];
        $content = Auth::user()['username']. ' 新建了讨论-'. $event['title'];

        $newNotify = $this->createNotification($event, $title, $content);
        $this->sendNotification($event['project_id'], $newNotify['id']);
    }

    /**
     * 更新讨论触发通知创建、通知发送
     * @param $event
     */
    public function discussionUpdated($event)
    {
        $title = $event['title'];
        $content = Auth::user()['username']. ' 更新了讨论-'. $event['title'];

        $newNotify = $this->createNotification($event, $title, $content);
        $this->sendNotification($event['project_id'], $newNotify['id']);
    }

    /**
     * 指派项目成员关注讨论触发通知创建、通知发送
     * @param $event
     */
    public function followerAdd($event)
    {
        $title = $event['title'];
        $content = Auth::user()['username']. ' 请求你关注讨论-'. $event['title'];

        $newNotify = $this->createNotification($event, $title, $content);
        $this->storeToInbox($newNotify['id'], $event['followers']);
    }

    protected function config()
    {
        $eloquentAction = [
            'created' => '@discussionCreated',
            'updated' => '@discussionUpdated'
        ];
        $this->setEloquentEvent($eloquentAction, 'ProjectDiscussion');
        $this->setEvent('addFollower', $this->handlerName. '@followerAdd');
    }

    /**
     * 创建新的通知，自动设置一些固定字段的值
     *
     * @param $event array 消息源
     * @param $title string 通知的标题
     * @param $content string 通知的内容
     * @return Illuminate\Database\Eloquent\Model 新创建的通知模型
     */
    protected function createNotification($event, $title, $content)
    {
        $notifyData['type_id'] = 4;
        $notifyData['trigger_id'] = Auth::user()['id'];
        $notifyData['source_id'] = $event['id'];
        $notifyData['project_id'] = $event['project_id'];

        $notifyData['title'] = $title;
        $notifyData['content'] = $content;

        $notifyData['created_at'] = date('Y-m-d');
        $notifyData['updated_at'] = date('Y-m-d');

        return Notification::create($notifyData);

    }

    /**
     * 给接受者发送通知
     *
     * @param $sourceId int 事件源的id
     * @param $notificationId int 通知的id
     */
    protected function sendNotification($sourceId, $notificationId)
    {

        $receiverIds = array_fetch(
            Project_Member::where('project_id', $sourceId)->get(['member_id'])->toArray(),
            'member_id'
        );

        array_push($receiverIds, Project::findOrFail($sourceId)['creater_id']);

        $this->storeToInbox($notificationId, $receiverIds);
    }

    protected $handlerName = 'ProjectDiscussionEventHandler';
}
