<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14-12-9
 * Time: 下午3:02
 */

namespace Libraries\EventHandler;

use Auth;
use Illuminate\Database\Eloquent\Model;
use User;
use Notification;
use Project;
use Project_Member;

class ProjectEventHandler extends AbstractEventHandler
{
    /**
     * 对应事件：eloquent.created :Project的处理方法
     * 消息的接受对象：项目的创建者和全体成员
     *
     * @param $event
     */
    public function projectUpdated($event)
    {
        $title = $event['name'];
        $content = Auth::user()['username']. '更新了项目';

        $newNotify = $this->createNotification($event, $title, $content);
        $this->sendNotification($event['id'], $newNotify['id']);
    }

    /**
     * 对应事件：eloquent.deleting :Project的处理方法
     * 消息的接受对象：项目的创建者和全体成员
     *
     * @param $event
     */
    public function projectDestroyed($event)
    {
        $title = $event['name'];
        $content = Auth::user()['username']. '删除了项目';

        $newNotify = $this->createNotification($event, $title, $content);
        $this->sendNotification($event['id'], $newNotify['id']);
    }

    /**
     * 配置事件订阅者所监听的事件和对应的处理方法
     */
    protected function config()
    {
        $eloquentAction = [
            'updated' => '@projectUpdated',
            'deleting' => '@projectDestroyed'
        ];
        $this->setEloquentEvent($eloquentAction, 'Project');
    }

    /**
     * 创建新的通知，自动设置一些固定字段的值
     *
     * @param $event array 消息源
     * @param $title string 通知的标题
     * @param $content string 通知的内容
     * @return Illuminate\Database\Eloquent\Model 新创建的通知模型
     */
    protected function createNotification($event, $title, $content)
    {
        $notifyData['type_id'] = 2;
        $notifyData['trigger_id'] = Auth::user()['id'];
        $notifyData['source_id'] = $event['id'];
        $notifyData['project_id'] = $event['id'];

        $notifyData['title'] = $title;
        $notifyData['content'] = $content;

        $notifyData['created_at'] = date('Y-m-d');
        $notifyData['updated_at'] = date('Y-m-d');

        return Notification::create($notifyData);

    }

    /**
     * 给接受者发送通知
     *
     * @param $sourceId int 事件源的id
     * @param $notificationId int 通知的id
     */
    protected function sendNotification($sourceId, $notificationId)
    {

        $receiverIds = array_fetch(
            Project_Member::where('project_id', $sourceId)->get(['member_id'])->toArray(),
            'member_id'
        );

        array_push($receiverIds, Project::findOrFail($sourceId)['creater_id']);

        $this->storeToInbox($notificationId, $receiverIds);
    }

    protected $handlerName = 'ProjectEventHandler';

}
