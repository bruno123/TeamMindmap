/**
 * Created by spatra on 14-12-14.
 */


define(['angular', 'personalNotificationJS/ng-require', 'angularMocks'], function(angular){

  //注入当前的ngModule
  beforeEach(module('TeamMindmap.personal.notification'));

  describe('Unit test: NotificationService', function(){

    var NotificationService, $httpBackend, $timeout;
    var testBaseUrl = 'api/notify';
    var testProjectId = 2;
    var mockData = [
      {
        "id": "1",
        "type_id": "类型id",
        "title": "通知标题",
        "content": "通知的内容",
        "created_at": "创建时间",
        "source_id": "通知源id",
        "trigger_id": "通知触发者id",
        "read": true,
        trigger: {

        }
      }
    ];

    beforeEach(inject(function(_NotificationService_, _$httpBackend_, _$timeout_){
      NotificationService = _NotificationService_;
      $timeout = _$timeout_;
      $httpBackend = _$httpBackend_;
    }));

    it('method: getNotificationOnProject', function(){
      $httpBackend.expectGET(testBaseUrl + '?project_id=' + testProjectId).respond(200, mockData);
      var mustCalled = jasmine.createSpy('success');

      NotificationService.getNotificationOnProject(testProjectId)
        .success(function(data){
          expect(data).toEqual(mockData);
          mustCalled();
        });

      $httpBackend.flush();
      $timeout.flush();
      expect(mustCalled).toHaveBeenCalled();

    });// End of --> method: getNotificationOnProject

    it('method: getNotificationsOnCond', function(){

      var mockScope = {};

      $httpBackend.expectGET(testBaseUrl + '?project_id=' + testProjectId).respond(200, mockData);

      NotificationService.getNotificationsOnCond({
        read: true,
        projectId: testProjectId
      }, mockScope);

      $httpBackend.flush();
      $timeout.flush();

      expect(mockScope['notificationList']).toEqual(mockData);
    });

  });// End of --> Unit test: NotificationService

});