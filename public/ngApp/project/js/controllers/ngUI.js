/**
 * Created by spatra on 14-12-4.
 */

define(['projectJS/module'], function(projectModule){

  /**
   * 此控制器用于操作新建任务的模态框
   */
  projectModule.controller('CreateTaskModalController',
    ['$scope', '$modalInstance', '$stateParams', 'NgUIDatePickerService', 'projectMemberList', 'priorityList','parentTaskInfo',
      function($scope, $modalInstance, $stateParams, NgUIDatePickerService, projectMemberList, priorityList, parentTaskInfo){
        /*
        初始化模态框中使用到的变量
         */
        $scope.taskData = {
          priority_id: 1
        };   //任务数据
        $scope.chosen = [];
        $scope.datePicker = NgUIDatePickerService.getDefault();   //时间选择器设定
        $scope.appointed_member = [];       //被添加的成员
        $scope.initProjectMembers = false;  //加载状态
        $scope.priorityList = priorityList; //加载任务优先级信息
        $scope.projectMembers = projectMemberList;  //加载当前项目的成员和创建者
        $scope.parentTaskInfo = parentTaskInfo; //如果存在父级任务，则此初存放信息，否则为null

        /**
         * 点击`取消`按钮时执行
         */
        $scope.cancel = function(){
          $modalInstance.dismiss('cancel');
        };

        /**
         * 点击`确定`按钮时执行
         */
        $scope.ok = function(){
          $scope.taskData.expected_at = $scope.datePicker.getDate();

          //转换数据，以符合前后端协定
          $scope.taskData.appointed_member = { 'add': [] };
          for(var member in $scope.appointed_member ){
            $scope.taskData.appointed_member['add'].push( $scope.appointed_member[member]['id'] );
          }

          $modalInstance.close( $scope.taskData );
        };
      }
  ]);//End of --> CreateTaskModalController

  /**
   * 此控制器用于操作新建 任务-讨论 的评论所对应的模态框
   */
  projectModule.controller('AddCommentModalController', ['$scope', '$modalInstance',
    function($scope, $modalInstance){
      $scope.content;

      $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
      };

      $scope.ok = function(){
        $modalInstance.close({
          content: $scope.content
        });
      };
  }]);
});