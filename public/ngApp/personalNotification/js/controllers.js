/**
 * Created by spatra on 14-12-12.
 */

define(['personalNotificationJS/module'], function(notificationModule){

  /**
   * 用于管理个人模块，个人通知的控制器
   */
  notificationModule.controller('NotificationController', ['$scope', '$rootScope', '$state', 'projectList', 'notificationList', 'ClassHelperService', 'NotificationService', 'ScrollService', 'PaginationService', 'typeLabelSet',
    function($scope, $rootScope, $state, projectList, notificationList, ClassHelperService, NotificationService, ScrollService, PaginationService, typeLabelSet){
      $scope.typeLabelSet = typeLabelSet;

      //初始化所使用到的变量
      $scope.projectList = projectList;
      $scope.notificationList = notificationList.data;
      $scope.pagination = PaginationService.createPagination({
        type: 'LoadingPagination',
        currentPage: 1,
        itemsPerPage: notificationList.per_page,
        totalItems: notificationList.total
      });



      //页面状态
      $scope.showCondition = {
        project_id: null,
        read: 0
      };

      $scope.readCondition = false;


      $scope.goToState = function(notification) {
        var systemId = 1;
        if(notification.type_id != systemId) {

          var curStateObj = NotificationService.getStateObj(notification, $scope.typeLabelSet);
          $state.go(curStateObj.stateName, curStateObj.stateParams);
        }
      };



      /**
       * 重新加载通知列表函数
       *
       */
      var reloadNotificationList = function() {
        //当前页设为1,每页的通知数为 currentPage*itemsPerPage
        var paramsObj = {
          per_page: $scope.pagination.itemsPerPage * $scope.pagination.currentPage,
          page: 1
        };

        ClassHelperService.update($scope.showCondition, paramsObj);
        NotificationService.accessor.get({
          params: paramsObj
        })
          .success(function(data){
            $scope.notificationList = data.data;

            $scope.pagination.totalItems = data.total;
            $scope.pagination.setShowLoading(false);
            $scope.pagination.showFinish();

          })
          .error(function(data){
            console.log(data.error);
          })
      };//End of--reloadNotificationList

      //select的重新加载
      $scope.reloadByProject = function(){
        $scope.pagination.setFirstCurrentPage();
        $scope.pagination.setFinishLoading(false);
        reloadNotificationList();
      };

      //read的脏值查询,如果有改变则重新加载
      $scope.$watch('readCondition', function(newVal, oldVal){
        if(newVal != oldVal) {
          //1为已读,0为未读
          $scope.showCondition.read = ($scope.readCondition == true ? 1 : 0);
          $scope.pagination.setFirstCurrentPage();
          $scope.pagination.setFinishLoading(false);
          reloadNotificationList();
        }

      });

      //设为已读
      $scope.setRead = function(notification){
        NotificationService.setRead(notification.id, true)
          .success(function(){
            reloadNotificationList();
            $scope.$emit('unread:update');
          })
          .error(function(data){
            console.error(data);
          })
      };



      ScrollService.init(angular.element('body'), 'load:notification');
      //滚动条到底部时的监听
      $rootScope.$on('load:notification', function(){
        $scope.$apply(function(){


          $scope.pagination.loadDataWhenToBottom(reloadNotificationList);

        });
      });



  }]);//End of --> NotificationController
});