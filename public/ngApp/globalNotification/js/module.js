/**
 * Created by rockyren on 14/12/10.
 */
define(['angular','angularAnimate','angularToasty','angularUIRouter'],function(angular){
  var globalNotificationModule = angular.module('TeamMindmap.globalNotification',['ngAnimate','toasty','ui.router']);

  return globalNotificationModule;
});