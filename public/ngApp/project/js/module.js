/**
 * Created by spatra on 14-12-2.
 */

define(['angular', 'commonJS/ng-require', 'mindmapJS/ng-require', 'angularUIRouter', 'angularAnimate', 'bxslider', 'angularBootstrap', 'angularBootstrapTemplate', 'ngFileUpload', 'angularDeckgrid'],

  function(angular, commonModule, mindmapModule){
    var projectModule = angular.module('TeamMindmap.project', [commonModule, mindmapModule, 'ui.router', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tpls', 'angularFileUpload', 'akoenig.deckgrid']);

    projectModule.constant('projectModuleBaseUrl', 'ngApp/project/');

    return projectModule;
});