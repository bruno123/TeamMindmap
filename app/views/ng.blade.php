<!doctype html>

<html lang="zh-cmn-hans" id="TeamMindmap">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title ng-bind="title" ></title>


{{-- default styles --}}
{{HTML::style('packages/bower/bootstrap/dist/css/bootstrap.min.css')}}


{{HTML::style('css/nav-style.css')}}




{{--插件样式--}}
{{HTML::style('packages/bower/bxslider-4/jquery.bxslider.css')}}
{{HTML::style('ngApp/common/localResizeIMG-2/build/localResize.css')}}
{{HTML::style('packages/bower/angular-toasty/css/ng-toasty.css')}}


{{--通用样式--}}
{{HTML::style('css/app-common.css')}}
{{HTML::style('css/ngCommon/third-nav-style.css')}}
{{HTML::style('css/ngCommon/label-list-box.css')}}

{{--片段样式--}}

{{HTML::style('ngApp/project/css/project-selection-common.css')}}
{{HTML::style('ngApp/project/css/create-task-style.css')}}
{{HTML::style('ngApp/project/css/member-selection-style.css')}}


{{--具体文件对应样式--}}
{{--project样式--}}
{{HTML::style('ngApp/project/css/project-list-style.css')}}
{{HTML::style('ngApp/project/css/project-member-style.css')}}
{{HTML::style('ngApp/project/css/project-creating-style.css')}}
{{HTML::style('ngApp/project/css/task-style.css')}}
{{HTML::style('ngApp/project/css/task-info-style.css')}}
{{HTML::style('ngApp/project/css/sidebar-style.css')}}
{{HTML::style('ngApp/project/css/project-discussion.css')}}


{{--personal样式--}}
{{HTML::style('ngApp/personal/css/information-common.css')}}
{{HTML::style('ngApp/personal/css/personal-template-style.css')}}
{{HTML::style('ngApp/personal/css/second-nav-style.css')}}
{{--HTML::style('ngApp/personal/css/third-nav-style.css')--}}

{{--personal.notification样式--}}
{{HTML::style('ngApp/personalNotification/css/notification-index-style.css')}}

{{-- personal.message --}}
{{HTML::style('ngApp/personalMessage/css/message-common.css')}}
{{HTML::style('ngApp/personalMessage/css/message-list.css') }}
{{HTML::style('ngApp/personalMessage/css/message-creating-style.css')}}
{{HTML::style('ngApp/personalMessage/css/message-show-style.css')}}


{{-- project.sharing样式 --}}
{{HTML::style('ngApp/project/css/sharing-creating.css')}}
{{HTML::style('ngApp/project/css/sharing-info.css')}}
{{HTML::style('ngApp/project/css/sharing-list.css')}}
{{HTML::style('ngApp/project/css/sharing-deckgrid.css')}}

{{-- mindmap样式 --}}
{{HTML::style('ngApp/mindmap/css/mindmap.css')}}

{{--font-awesome字符库可能会与其他样式库冲突,所以放在最后--}}
{{HTML::style('packages/bower/font-awesome/css/font-awesome.css')}}

{{--Bootstrap Markdown的样式文件 --}}
{{HTML::style('packages/bower/bootstrap-markdown/css/bootstrap-markdown.min.css') }}



<body>


<main-top-nav function-nav-items="functionNavItems"></main-top-nav>

<ui-view></ui-view>

@section('script')
    {{HTML::script('/packages/bower/requirejs/require.js',array('data-main'=>'../ngApp/ng-require-mainApp.js'))}}
@show
</body>
</html>