/**
 * Created by spatra on 14-12-25.
 */

define(['projectJS/module'], function(projectModule){

  /**
   * 用于控制讨论模块的列表及相关显示
   */
  projectModule.controller('DiscussionListController', ['$scope', '$stateParams', 'discussionList', 'DiscussionService', 'discussionFilterConditions', 'ClassHelperService', 'PaginationService',
    function($scope, $stateParams, discussionList, DiscussionService, discussionFilterConditions, ClassHelpService, PaginaitionService){
      /*
       初始化用到的变量
       */
      $scope.discussionList = discussionList.data;
      $scope.conditionObj = {};
      $scope.currentProjectId = $stateParams.projectId;
      $scope.conditions = discussionFilterConditions;

      $scope.pagination = PaginaitionService.createPagination({
        type: 'NumberPagination',
        currentPage: discussionList.current_page,
        itemsPerPage: discussionList.per_page,
        totalItems: discussionList.total
      });


      //设置父级资源id
      DiscussionService.accessor['setParentResourceId']($stateParams);

      //重新加载讨论列表
      var reloadDiscussion = function(){
        var paramsObj = {
          per_page: $scope.pagination.itemsPerPage,
          page: $scope.pagination.currentPage
        };

        ClassHelpService.update($scope.conditionObj, paramsObj);

        DiscussionService.accessor.get({
          params: paramsObj
        })
          .success(function(data){
            $scope.discussionList = data.data;

            $scope.pagination.totalItems = data.total;

            //此处协定当筛选条件的值为０是的时候，实质上代表没有过滤
            $scope.withoutFiltering = true;
            for(var cond in $scope.conditionObj ){
              if( $scope.conditionObj[cond] ){
                $scope.withoutFiltering = false;
                break;
              }
            }
          })
          .error(function(data){
            console.error(data);
            $scope.$emit('message:error', {
              title: '出错了',
              msg: data.error
            });
          });
      };

      $scope.getCurPageDiscussion = reloadDiscussion;

      //筛选条件改变时，重新加载讨论列表
      $scope.$watch('conditionObj', function(newValue, oldValue){
        $scope.pagination.setFirstCurrentPage();
        reloadDiscussion();
      }, true);


  }]);//End of --> DiscussionListController

  /**
   * 用于控制新增讨论的控制器
   */
  projectModule.controller('DiscussionCreatingController', ['$rootScope', '$scope', '$stateParams', '$state', 'DiscussionService', 'userList',
    function($rootScope, $scope, $stateParams, $state, DiscussionService, userList){

      DiscussionService.accessor['setParentResourceId']($stateParams);

      //重置待添加的讨论对象（清空）
      function resetDiscussion(){
        $scope.addDiscussion = {
          followers: []
        };//待添加的讨论
      }

      /*
        初始化用到的变量
       */
      $scope.currentProjectId = $stateParams.projectId;   //当前所属项目的id
      $scope.userList = userList;   //当前项目的成员及创建者，用于生成请求关注列表
      resetDiscussion();

      //执行添加讨论的操作
      $scope.add = function(){

        var followersId = [];

        for(var i = 0; i < $scope.addDiscussion.followers.length; ++i ){
          followersId.push($scope.addDiscussion.followers[i]['id']);
        }

        DiscussionService.accessor.store({
          'title': $scope.addDiscussion.title,
          'content': $scope.addDiscussion.content,
          'followers': followersId
        })
          .success(function(){
            $state.go('project.show.discussion', $stateParams);
            $rootScope.$emit('message:success',{
              'title': '操作成功',
              'msg': '讨论已经成功添加'
            });
          })
          .error(function(data){
            console.error(data);
            $scope.$emit('message:error', {
              title: '出错',
              mgs: data.error || '操作失败'
            });
          });

      };//End of --> add

      //重置讨论的内容
      $scope.reset = resetDiscussion;
  }]);//End of --> DiscussionCreatingController

  /**
   * 此控制器对应查询具体的讨论信息
   */

  projectModule.controller('DiscussionInfoController', ['$scope', '$rootScope', '$stateParams', '$modal', 'projectModuleBaseUrl', 'currentDiscussion', 'DiscussionService', 'ScrollService', 'commentList', 'CommentService', 'PaginationService',
    function($scope, $rootScope, $stateParams, $modal, projectModuleBaseUrl, currentDiscussion, DiscussionService, ScrollService, commentList, CommentService, PaginationService) {

      DiscussionService.accessor['setParentResourceId']($stateParams);

      /*
       初始化使用到的变量
       */
      $scope.currentProjectId = $stateParams.projectId;
      $scope.currentDiscussion = currentDiscussion;
      $scope.commentList = commentList.data;   //评论列表

      $scope.pagination = PaginationService.createPagination({
        type: 'LoadingPagination',
        currentPage: 1,
        itemsPerPage: commentList.per_page,
        totalItems: commentList.total
      });

      //重新加载讨论信息
      function reloadDiscussion() {
        DiscussionService.accessor.show($stateParams['discussionId'])
          .success(function (data) {
            $scope.currentDiscussion = data;
          })
          .error(function (data) {
            console.error(data);
            $scope.$emit('message:error', {
              title: '出错',
              msg: '重新加载讨论信息失败'
            });
          });
      }

      /*
       重新加载评论信息
       */
      function reloadComment() {
        CommentService.accessor.get({
          params: {
            per_page: $scope.pagination.itemsPerPage * $scope.pagination.currentPage,
            page: 1
          }
        })
          .success(function (data) {
            $scope.commentList = data.data;

            $scope.pagination.totalItems = data.total;
            $scope.pagination.setShowLoading(false);
            $scope.pagination.showFinish();

          })
          .error(function (data) {
            console.log(data.error);
          });
      }

      /*
       打开添加评论的模态框，并进行相关的后台交互
       */
      $scope.addComment = function () {
        /*
         生成模态框
         */
        $scope.addCommentModal = $modal.open({
          templateUrl: projectModuleBaseUrl + 'tpls/add-discussion-comment-modal.html',
          controller: 'AddCommentModalController',
          size: 'lg'
        });

        /*
         如果选择了确定，则模态框关闭后执行提交操作
         */
        $scope.addCommentModal.result
          .then(function (data) {

            CommentService.accessor.store(data)
              .success(function (data) {
                reloadComment();
                $scope.$emit('unread:update');
                $scope.$emit('message:success', {
                  title: '操作成功',
                  msg: '评论已成功添加'
                });
              })
              .error(function (data) {
                console.error(data);
                $scope.$emit('message:error', {
                  title: '操作失败',
                  msg: data.error
                });
              });
          });
      };

      /*
       开启或关闭讨论
       */
      $scope.toggleOpenStatus = function () {
        var reverseStatus = !$scope.currentDiscussion.baseInfo.open;

        DiscussionService.accessor.update($stateParams['discussionId'], {
          'open': reverseStatus
        })
          .then(function () {
            $scope.currentDiscussion.baseInfo.open = reverseStatus;
            $scope.$emit('unread:update');
            $scope.$emit('message:success', {
              title: '操作成功',
              msg: '讨论状态已更改'
            });
          }, function (resp) {
            console.error(resp.data);
            $scope.$emit('message:error', {
              title: '操作失败',
              msg: resp.data.error
            });
          });
      };//End of --> toggleOpenStatus

      ScrollService.init(angular.element('body'), 'load:comment');

      $rootScope.$on('load:comment', function () {
        $scope.$apply(function () {
          $scope.pagination.loadDataWhenToBottom(reloadComment);

        });
      });

    }]);//End of --> DiscussionInfoController

});