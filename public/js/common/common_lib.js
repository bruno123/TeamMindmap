/**
 * Created by rockyren on 14/11/4.
 */
define(['jquery'],function($){
  /**
   * 实现导航栏高亮
   */
  var highlight = function(){
    var base_url = window.location.protocol + '//' + window.location.host;
    var current_url = window.location.href;
    var current_model = current_url.replace(base_url,'').split('/')[1];

    $('#nav-function li').each(function(){
      //alert($(this).text());
      var link_url = $(this).find('a').attr('href');
      var link_model = link_url.replace(base_url,'').split('/')[1];

      if(current_model.indexOf(link_model) >= 0) {
        $(this).addClass('active');
      }

    });
  };

  return {
    highlight: highlight
  }
});