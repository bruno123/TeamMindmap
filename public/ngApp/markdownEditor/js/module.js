/**
 * Created by spatra on 14-12-21.
 */

/**
 * 此模块为项目提供Markdown编辑和预览的通用 ng directive
 */

define(['angular'], function(angular){

  var markdownEditorModule = angular.module('TeamMindmap.markdownEditor', []);

  return markdownEditorModule;
});